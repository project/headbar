<?php

namespace Drupal\headbar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Render\Renderer;

/**
 * Provides a 'Headbar' block.
 *
 * @Block(
 *   id = "headbar_block",
 *   admin_label = @Translation("Headbar")
 * )
 */
class HeadbarBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a Drupalist object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current_user.
   * @param Drupal\Core\Config\ConfigFactory $configFactory
   *   The config.factory.
   * @param Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected AccountInterface $currentUser,
    protected ConfigFactory $configFactory,
    protected Renderer $renderer,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    // Check if current user has permission to view headbar.
    if ($this->currentUser->hasPermission('view headbar')) {
      // Get configuration values from headbar config form.
      $config = $this->configFactory->getEditable('headbar.configure');
      $colour = $config->get('msg_color');
      $color_hover = $config->get('msg_color_hover');
      $delaytime = $config->get('msg_delaytime');
      // Add/Attach JS file.
      $build['#attached']['library'][] = 'headbar/headbar.script';
      // Pass Settings to JS file.
      $build['#attached']['drupalSettings']['headbar']['colour'] = $colour;
      $build['#attached']['drupalSettings']['headbar']['color_hover'] = $color_hover;
      $build['#attached']['drupalSettings']['headbar']['delaytime'] = $delaytime;
      // Add/Attach CSS file.
      $build['#attached']['library'][] = 'headbar/headbar.theme';
      // Attach template file using #theme.
      $build['#theme'] = ['headbar_block'];
      $this->renderer->addCacheableDependency($build, $config);
    }
    return $build;
  }

}

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * Author/Maintainers

INRODUCTION
-----------

 * This module provides a bar which will appear on the header of the page after given time.

REQUIREMENTS
------------

 * None.

INSTALLATION
------------

 * To install, copy the headbar directory and all its contents to your sites/all/modules
  directory.

 * To enable the module go to Administer > Modules, and enable "HeadBar".

CONFIGURATION
-------------
 * Go To admin/config/user-interface/headbar

 * Add the text message which will appear on the headbar, Color and Delay time.

 * Go to admin/structure/block and enable "Headbar" block in any of the theme region.

 * Go to admin/people/permissions and configure permissions for the "HeadBar" visibility and administration.

TROUBLESHOOTING
---------------

 * As an admin of the site you will not be able to see the headbar if "admin menu" is visible.

AUTHOR/MAINTAINERS
------------------
Author:
 * Sumit Kumar(Sumit kumar) - https://www.drupal.org/user/2445096/

Current maintainers:
 * Prashant Chauhan(Prashant.c) - https://www.drupal.org/user/1936756/
